# Recipe App API Proxy

NGINX proxy app for out recipe app API

## Usage

### Enviroment Variables

* `LISTEN_PORT` - port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (defauly : `9000`)
